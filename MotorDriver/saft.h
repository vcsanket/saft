#define XMAX 100
#define XMIN 0
#define YMAX 200
#define YMIN 0
#define INTERVAL 10 //ms

class Point {
private:
        double xval;
        double yval;
public:
	// Constructor
        Point(double x=0.0, double y=0.0);

        // Getters
        double x();
        double y();

        // Distance to another point.
        double dist(Point other);

        // Move the existing point by diaplacements a and b
        void move(double a, double b);

        // returns the direction of travel based on previous point
        double direction(Point previous);
};

class Range {
private:
	Point p1;
	Point p2;
public:
	// Constructor
	Range(Point a, Point b);

	// returns a number which gives how close a point p is to this Range
	int in_range(Point p);	

	void set_new_range(Point a, Point b);
};

class Ball {
/* 
	This class tracks the movement of the ball and gives out its posible next position
*/
private:
	Point previous_position;
	double direction

public:
	// Constructor
	Ball(Point p);
	// Setter for previous_position
	void set_position(Point p);

	// Based on previous_position returns the next possible position of the ball
	Point get_next_position();
};

class Rod {
 private:
 	double displacement; // Gives the current displacement of the rod
 	Range players[3]; // Current range of the 3 players on rod
 	Motor m;

public:
	// Move the motors based on next position p
	// This function first checks if any players of this rod are currently in range to take action
	// If they are in range then rotate the rods.
	// If not in range, also determine optimum lateral movement followed by roatational movement
	void take_action(Point next_pos);
};

class Motor {

public:
	void lateral_control();
	void roatational_control();
};
